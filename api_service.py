from flask import Flask, jsonify, request
from flask_cors import CORS
from smtp_util import smtp_util
import json
import logging

app = Flask(__name__)
CORS(app)



@app.route("/")
def index():
    return 'OK!'




@app.route("/email/", methods=['POST'])
@app.route("/email", methods=['POST'])
def email_handler():
    data = request.get_data()
    file = request.files['file'] #hardcoded for now
    email = request.form["email_id"]
    message = request.form["email_content"]
    subject = request.form["subject"]
    filename = request.form["filename"]
    file_content = file.read()
    a = smtp_util()
    response = {'response':200}
    try:
         a.email(email, subject, message, file_content, filename)
    except(Exception): response = {'response':400}
    response_string = json.dumps(response)
    return response_string


@app.route("/email_without_file/", methods=['POST'])
@app.route("/email_without_file", methods=['POST'])
def email_handler_without_file():
    data = request.get_data()
    #file = request.files['file'] #hardcoded for now
    email = request.form["email_id"]
    message = request.form["email_content"]
    subject = request.form["subject"]
    #filename = request.form["filename"]
    #file_content = file.read()
    a = smtp_util()
    response = {'response':200}
    try:
         a.email_without_file(email, subject, message)
    except Exception as e: 
        response = {'response':400}
        logging.warn(e)
    response_string = json.dumps(response)
    return response_string


if __name__ == "__main__":
    app.run(debug=True)
