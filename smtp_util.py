
import smtplib
import email_server_creds as creds
import logging
from email.message import EmailMessage #pip install email
from email import encoders
from email.mime.multipart import MIMEMultipart
#from email.message import Message

from email.mime.text import MIMEText
from email.mime.base import MIMEBase




class smtp_util(object):
    
    def email(self, receiver_email_address, email_subject, email_message, file, filename):
        msg = self.form_email_content(receiver_email_address, email_subject, email_message, file, filename)
        smtp = self.smtp_initialization()
        smtp.sendmail(creds.sftp_email_id, receiver_email_address, msg)
        smtp.quit()


    def smtp_initialization(self):
        server = smtplib.SMTP(creds.gmail_sftp_server, creds.sftp_server_port)
        server.ehlo()
        server.starttls()
        email = creds.sftp_email_id
        password = creds.sftp_email_id_password
        try:
            server.login(email, password)
        except(smtplib.SMTPAuthenticationError):
            logging.error('outbound email set up not done correctly, check myaccount.google.com/lesssecureapps')
            return None
        return server
    

    def form_email_content(self, receiver_email_address, email_subject, email_message, file, filename):
        msg = MIMEMultipart()
        msg['From'] = creds.sftp_email_id
        msg['To'] = receiver_email_address
        msg['Subject'] = email_subject
        msg.attach(MIMEText(email_message, 'plain'))
        logging.warn('From:::'+msg['From'])
        logging.warn('To:::'+msg['To'])
        logging.warn('Subject:::'+msg['Subject'])
        logging.warn('email_message:::'+email_message)
        #logging.warn('file:::'+file.read())
        #print('type(file)')
        #print(type(file))
        #print(len(file))
        part = MIMEBase('application','octet-stream')
        part.set_payload(file)
        encoders.encode_base64(part)
        #print('---------------------------encoder set')
        part.add_header('Content-Disposition',"attachment; filename= " + filename)
        msg.attach(part)
        text = msg.as_string()
        return text

    def email_without_file(self, receiver_email_address, subject, message):
        msg = self.form_email_content_without_file(receiver_email_address, subject, message)
        smtp = self.smtp_initialization()
        smtp.sendmail(creds.sftp_email_id, receiver_email_address, msg)
        smtp.quit()


    def form_email_content_without_file(self, receiver_email_address, email_subject, email_message):
        msg = MIMEMultipart()
        msg['From'] = creds.sftp_email_id
        msg['To'] = receiver_email_address
        msg['Subject'] = email_subject
        msg.attach(MIMEText(email_message, 'plain'))
        logging.warn('From:::'+msg['From'])
        logging.warn('To:::'+msg['To'])
        logging.warn('Subject:::'+msg['Subject'])
        logging.warn('email_message:::'+email_message)
        #logging.warn('file:::'+file.read())
        #print('type(file)')
        #print(type(file))
        #print(len(file))
        #part = MIMEBase('application','octet-stream')
        #part.set_payload(file)
        #encoders.encode_base64(part)
        #print('---------------------------encoder set')
        #part.add_header('Content-Disposition',"attachment; filename= " + filename)
        #msg.attach(part)
        text = msg.as_string()
        return text